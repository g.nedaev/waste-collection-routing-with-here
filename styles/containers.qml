<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" version="3.4.4-Madeira" simplifyLocal="1" simplifyDrawingHints="0" simplifyMaxScale="1" maxScale="0" simplifyAlgorithm="0" readOnly="0" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="RuleRenderer" symbollevels="1" forceraster="0" enableorderby="0">
    <rules key="{ac15d8eb-1ca5-44ef-816b-80f9a846939b}">
      <rule description="For when on fire" symbol="0" label="fire" filter=" &quot;is_fire&quot; is true" key="{4853e65e-7841-4298-83a4-b2a3b2cbbc3b}"/>
      <rule symbol="1" label="not full" filter="ELSE" key="{60c594ba-649b-49c3-9cfc-f3a15d4e8a80}"/>
      <rule description="when not positioned upright" symbol="2" label="not upright" filter=" &quot;is_fall_over&quot; is true" checkstate="0" key="{e9b00fff-06b2-41ee-9c42-20b3f7df680c}"/>
      <rule description="for full containers" symbol="3" label="full" filter="&quot;is_full&quot; is true" checkstate="0" key="{60e5078c-da0c-4216-bfec-3b9286156cf4}"/>
      <rule symbol="4" label="empty battery" filter="&quot;is_battery_low&quot; is true" key="{3d76a7b0-7ade-4961-a57a-0e927d4b8e73}"/>
    </rules>
    <symbols>
      <symbol type="marker" alpha="1" name="0" clip_to_extent="1" force_rhr="0">
        <layer pass="4" class="SimpleMarker" enabled="1" locked="0">
          <prop v="34" k="angle"/>
          <prop v="255,127,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="star" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="255,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="area" k="scale_method"/>
          <prop v="6" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <effect type="effectStack" enabled="0">
            <effect type="drawSource">
              <prop v="0" k="blend_mode"/>
              <prop v="2" k="draw_mode"/>
              <prop v="1" k="enabled"/>
              <prop v="1" k="opacity"/>
            </effect>
          </effect>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" alpha="1" name="1" clip_to_extent="1" force_rhr="0">
        <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
          <prop v="0" k="angle"/>
          <prop v="84,176,74,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="61,128,53,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.4" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" alpha="1" name="2" clip_to_extent="1" force_rhr="0">
        <layer pass="2" class="SimpleMarker" enabled="1" locked="0">
          <prop v="0" k="angle"/>
          <prop v="45,27,248,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="filled_arrowhead" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="area" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" alpha="1" name="3" clip_to_extent="1" force_rhr="0">
        <layer pass="1" class="SimpleMarker" enabled="1" locked="0">
          <prop v="0" k="angle"/>
          <prop v="219,30,42,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="128,17,25,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.4" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" alpha="1" name="4" clip_to_extent="1" force_rhr="0">
        <layer pass="3" class="SimpleMarker" enabled="1" locked="0">
          <prop v="0" k="angle"/>
          <prop v="149,149,149,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="247,247,247,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.4" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>device_id</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="qgis2web/Visible" value="true"/>
    <property key="qgis2web/popup/bin_type_id" value="inline label"/>
    <property key="qgis2web/popup/dev_id" value="inline label"/>
    <property key="qgis2web/popup/device_name" value="inline label"/>
    <property key="qgis2web/popup/height_mm" value="no label"/>
    <property key="qgis2web/popup/is_battery_low" value="no label"/>
    <property key="qgis2web/popup/is_fall_over" value="no label"/>
    <property key="qgis2web/popup/is_fire" value="no label"/>
    <property key="qgis2web/popup/is_full" value="no label"/>
    <property key="qgis2web/popup/percentage_full" value="no label"/>
    <property key="qgis2web/popup/time_received" value="no label"/>
    <property key="qgis2web/popup/volume_l" value="no label"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory minScaleDenominator="0" backgroundAlpha="255" penAlpha="255" barWidth="5" sizeType="MM" lineSizeType="MM" height="15" scaleDependency="Area" opacity="1" labelPlacementMethod="XHeight" sizeScale="3x:0,0,0,0,0,0" penWidth="0" rotationOffset="270" width="15" lineSizeScale="3x:0,0,0,0,0,0" maxScaleDenominator="1e+08" enabled="0" diagramOrientation="Up" minimumSize="0" scaleBasedVisibility="0" backgroundColor="#ffffff" penColor="#000000">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings dist="0" priority="0" linePlacementFlags="18" zIndex="0" obstacle="0" placement="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="dev_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="device_name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bin_type_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="time_received">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="percentage_full">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="height_mm">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="volume_l">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="is_full">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="is_fire">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="is_fall_over">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="is_battery_low">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="dev_id" index="0" name=""/>
    <alias field="device_name" index="1" name=""/>
    <alias field="bin_type_id" index="2" name=""/>
    <alias field="time_received" index="3" name=""/>
    <alias field="percentage_full" index="4" name=""/>
    <alias field="height_mm" index="5" name=""/>
    <alias field="volume_l" index="6" name=""/>
    <alias field="is_full" index="7" name=""/>
    <alias field="is_fire" index="8" name=""/>
    <alias field="is_fall_over" index="9" name=""/>
    <alias field="is_battery_low" index="10" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="dev_id" expression="" applyOnUpdate="0"/>
    <default field="device_name" expression="" applyOnUpdate="0"/>
    <default field="bin_type_id" expression="" applyOnUpdate="0"/>
    <default field="time_received" expression="" applyOnUpdate="0"/>
    <default field="percentage_full" expression="" applyOnUpdate="0"/>
    <default field="height_mm" expression="" applyOnUpdate="0"/>
    <default field="volume_l" expression="" applyOnUpdate="0"/>
    <default field="is_full" expression="" applyOnUpdate="0"/>
    <default field="is_fire" expression="" applyOnUpdate="0"/>
    <default field="is_fall_over" expression="" applyOnUpdate="0"/>
    <default field="is_battery_low" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="dev_id" notnull_strength="1" constraints="3" unique_strength="1" exp_strength="0"/>
    <constraint field="device_name" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="bin_type_id" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="time_received" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="percentage_full" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="height_mm" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="volume_l" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="is_full" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="is_fire" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="is_fall_over" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="is_battery_low" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="dev_id" exp="" desc=""/>
    <constraint field="device_name" exp="" desc=""/>
    <constraint field="bin_type_id" exp="" desc=""/>
    <constraint field="time_received" exp="" desc=""/>
    <constraint field="percentage_full" exp="" desc=""/>
    <constraint field="height_mm" exp="" desc=""/>
    <constraint field="volume_l" exp="" desc=""/>
    <constraint field="is_full" exp="" desc=""/>
    <constraint field="is_fire" exp="" desc=""/>
    <constraint field="is_fall_over" exp="" desc=""/>
    <constraint field="is_battery_low" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="&quot;is_battery_low&quot;" sortOrder="1">
    <columns>
      <column hidden="0" type="field" name="time_received" width="-1"/>
      <column hidden="0" type="field" name="is_full" width="-1"/>
      <column hidden="0" type="field" name="is_fire" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column hidden="0" type="field" name="dev_id" width="-1"/>
      <column hidden="0" type="field" name="device_name" width="-1"/>
      <column hidden="0" type="field" name="bin_type_id" width="-1"/>
      <column hidden="0" type="field" name="percentage_full" width="-1"/>
      <column hidden="0" type="field" name="height_mm" width="-1"/>
      <column hidden="0" type="field" name="volume_l" width="-1"/>
      <column hidden="0" type="field" name="is_fall_over" width="-1"/>
      <column hidden="0" type="field" name="is_battery_low" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="bin_type_id" editable="1"/>
    <field name="dev_id" editable="1"/>
    <field name="device_id" editable="1"/>
    <field name="device_name" editable="1"/>
    <field name="height_mm" editable="1"/>
    <field name="is_battery_empty" editable="1"/>
    <field name="is_battery_low" editable="1"/>
    <field name="is_fall_over" editable="1"/>
    <field name="is_fire" editable="1"/>
    <field name="is_full" editable="1"/>
    <field name="is_not_upright" editable="1"/>
    <field name="level_percent" editable="1"/>
    <field name="percentage_full" editable="1"/>
    <field name="time_received" editable="1"/>
    <field name="volume_l" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="bin_type_id"/>
    <field labelOnTop="0" name="dev_id"/>
    <field labelOnTop="0" name="device_id"/>
    <field labelOnTop="0" name="device_name"/>
    <field labelOnTop="0" name="height_mm"/>
    <field labelOnTop="0" name="is_battery_empty"/>
    <field labelOnTop="0" name="is_battery_low"/>
    <field labelOnTop="0" name="is_fall_over"/>
    <field labelOnTop="0" name="is_fire"/>
    <field labelOnTop="0" name="is_full"/>
    <field labelOnTop="0" name="is_not_upright"/>
    <field labelOnTop="0" name="level_percent"/>
    <field labelOnTop="0" name="percentage_full"/>
    <field labelOnTop="0" name="time_received"/>
    <field labelOnTop="0" name="volume_l"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>device_id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
