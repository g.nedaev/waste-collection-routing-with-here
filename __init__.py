# -*- coding: utf-8 -*-

# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load Containers class from file Containers"""
    from .lib.containers import Containers
    return Containers(iface)
