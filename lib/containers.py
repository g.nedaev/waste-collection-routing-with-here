# -*- coding: utf-8 -*-

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMessageBox
from .resources import *
from .containers_dialog import ContainersDialog
import os
import requests
from qgis.core import QgsDataSourceUri, QgsProject, QgsVectorLayer, QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsMessageLog, QgsFeature, QgsGeometry, QgsPoint, QgsFeatureRequest, QgsExpression


class Containers:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """ Constructor """
        self.pluginDirectory = os.path.join(os.path.dirname(
            __file__), '..')  # one level above this file's directory

        # HERE credentials, env vars must be set!
        self.ID = os.environ['HERE_APP_ID']
        self.code = os.environ['HERE_APP_CODE']
        # de Steiger: is this the depot?
        self.depot = '52.34721,5.22194'
        self.mode = 'fastest;truck;traffic:enabled'
        # 2 minutes, in fact unless it varies depending on the container type, there's no need for it
        self.serviceTimePerContainer = 120
        # limits the behavior of extractFull function
        self.pointLimit = 48
        self.uriOptimizeWaypoints = f'https://wse.api.here.com/2/findsequence.json?app_id={self.ID}&app_code={self.code}&start={self.depot}&end={self.depot}&mode={self.mode}&departure=now'
        self.uriCalculateRoute = f"https://route.api.here.com/routing/7.2/calculateroute.json?app_id={self.ID}&app_code={self.code}&start={self.depot}&end={self.depot}&mode={self.mode}&departure=now&routeAttributes=sh"
        # limit 23 container locations
        self.uriNavigationGoogle = f"https://www.google.com/maps/dir/{self.depot}"
        # limit 48 container locations
        self.uriNavigationHere = f"https://wego.here.com/directions/mix/{self.depot}"

        # PyQGIS constants
        self.iface = iface
        self.project = QgsProject.instance()
        self.dlg = ContainersDialog()
        self.mainWindow = iface.mainWindow()
        self.actions = []
        self.menu = 'Containers'
        self.toolbar = self.iface.addToolBar('Containers')
        self.toolbar.setObjectName('toolbarContainers')

        # CRS for tranformation
        self.wgs84 = QgsCoordinateReferenceSystem(4326)
        self.rdnew = QgsCoordinateReferenceSystem(28992)
        self.reprojection = QgsCoordinateTransform(
            self.rdnew, self.wgs84, self.project)

        # Load containers from Postgres
        uriContainers = QgsDataSourceUri()
        uriContainers.setConnection(
            '136.144.184.51', '5432', 'iot_almere', 'postgres', 'vuilnisman101')
        uriContainers.setDataSource(
            'mock_iot_almere', 'latest_uplinks', 'dev_location_geom', '', 'dev_id')
        self.layerContainers = QgsVectorLayer(
            uriContainers.uri(False), 'containers', 'postgres')
        if not self.layerContainers.isValid():
            QMessageBox.critical(
                self.mainWindow, 'Containers plugin', 'Error loading container layer')

    def add_action(self, icon_path, text, callback, enabled_flag=True, parent=None):
        """Create clickable icons"""
        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)
        self.toolbar.addAction(action)
        self.iface.addPluginToMenu(self.menu, action)
        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        power_path = ':/plugins/containers/icons/power.png'
        route_path = ':/plugins/containers/route.png'

        self.add_action(
            power_path,
            text='Almere Containers',
            callback=self.showContainers,
            parent=self.mainWindow
        )

        self.add_action(
            route_path,
            text='Route',
            callback=self.route,
            parent=self.mainWindow
        )

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu('Containers', action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def showContainers(self):
        """Add styled container layer"""
        self.project.addMapLayer(self.layerContainers)
        self.layerContainers.loadNamedStyle(os.path.join(
            self.pluginDirectory, 'styles', 'containers.qml'))

    def extractFull(self):
        """Creates a list of nested lists which are lat/lon pairs of coordinates for all the full containers atm"""
        req = QgsFeatureRequest(QgsExpression(
            'is_full is true'))
        req.setLimit(self.pointLimit)
        full = self.layerContainers.getFeatures(req)
        full_wgs84 = [self.reprojection.transform(
            f.geometry().asPoint()) for f in full]

        return [[f.y(), f.x()] for f in full_wgs84]

    def optimizeWaypoints(self, locationList):
        """Calls Here's Fleet Telematics Waypoint Sequence API. Returns an array of waypoint objects (see API reference)"""
        # form the URI
        uri = self.uriOptimizeWaypoints
        for location in locationList:
            uri += f'&destination{locationList.index(location)}={location[0]},{location[1]};st:{self.serviceTimePerContainer}'
        # send request and debug
        response = requests.get(uri)
        if response.status_code != 200:
            QMessageBox.critical(self.mainWindow, 'Waypoint Optimization Request',
                                 f"Status: {response.status_code}\nMessage:{response.text}")
            return
        # return the result
        # sorts by their 'sequence' attribute
        return sorted(response.json()['results'][0]['waypoints'], key=lambda el: el['sequence'])

    def calculateRoute(self, waypointList):
        """Calls Here's Calculate Route API"""
        # form the URI
        uri = self.uriCalculateRoute
        for waypoint in waypointList:
            uri += f"&waypoint{waypointList.index(waypoint)}={waypoint}"
        # send request and debug
        response = requests.get(uri)
        if response.status_code != 200:
            QMessageBox.critical(
                self.mainWindow, 'Waypoint Optimization Request', f"{response.json()['details']}")
            return

        # transform the response into a list of lists with coordinates in QGIS format [lon, lat]
        route = response.json()['response']['route'][0]
        # of form: ["51.1,5.2","52.3,5.1"...]
        coordinatesAsStrings = route['shape']
        coordinatesAsSplitStrings = [coordinate.split(
            ',') for coordinate in coordinatesAsStrings]  # returns ['lat', 'lon']
        coordinatesQGISFormat = [[float(coordinate[1]), float(
            coordinate[0])] for coordinate in coordinatesAsSplitStrings]  # returns [lon, lat]

        return {
            'shape': coordinatesQGISFormat,
            'distance': route['summary']['distance'],
            'duration': route['summary']['travelTime']
        }

    def displayRoute(self, coordinateList, routeAttributes):
        """Creates the route geometry, adds it to a new memory layer and displays it"""
        pointList = [QgsPoint(i[0], i[1]) for i in coordinateList]
        feature = QgsFeature()
        feature.setGeometry(QgsGeometry.fromPolyline(pointList))
        feature.setAttributes([routeAttributes['distance'], routeAttributes['duration'],
                               routeAttributes['google'], routeAttributes['here']])
        layer = QgsVectorLayer(
            'LineString?crs=epsg:4326&field=distance(km):double&field=duration:string&field=google&field=here&index=yes',
            'route',
            'memory'
        )
        layer.startEditing()
        layer.addFeature(feature)
        layer.commitChanges()
        self.project.addMapLayer(layer)
        layer.loadNamedStyle(os.path.join(
            self.pluginDirectory, 'styles', 'route.qml'))

    def makeNavigationURIGoogle(self, coordinateList):
        """Only works with the max of 23 coordinates"""
        uri = self.uriNavigationGoogle
        coordinateListforGoogle = coordinateList[:23]

        for coordinate in coordinateListforGoogle:
            uri += f"/{coordinate}"

        uri += f"/{self.depot}"

        return uri

    def makeNavigationURIHere(self, coordinateList):
        """Only works with the max of 48 coordinates"""
        uri = self.uriNavigationHere
        coordinateListforHere = coordinateList[:48]

        for coordinate in coordinateListforHere:
            uri += f"/{coordinate}"

        uri += f"/{self.depot}"

        return uri

    def route(self):
        """Does all routing"""
        pts = self.extractFull()
        waypoints = self.optimizeWaypoints(pts)  # 10 requests a day
        # coordinates as lat/lon tuples in the right order
        orderedWaypoints = [
            f"{waypoint['lat']},{waypoint['lng']}" for waypoint in waypoints]

        navigationURIGoogle = self.makeNavigationURIGoogle(orderedWaypoints)
        navigationURIHere = self.makeNavigationURIHere(orderedWaypoints)

        # returns an object with keys 'shape', 'duration' and 'distance'
        route = self.calculateRoute(orderedWaypoints)

        routeAttributes = {
            'distance': round((route['distance'] / 1000), 2),
            # change to a proper time format
            'duration': f"{route['duration'] // 3600}:{route['duration'] // 60 % 60}:{route['duration'] % 60}",
            'google': navigationURIGoogle,
            'here': navigationURIHere
        }

        self.displayRoute(route['shape'], routeAttributes)
